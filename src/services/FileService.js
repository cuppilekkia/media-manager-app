import Api from '@/services/Api';

export default {
  getFile(key, filename) {
    const params = [];
    if (filename) {
      params.push(`key=${filename}`);
    }

    let string;
    if (params.length) {
      string = `?${params.join('&')}`;
    }
    return Api(key).get(`/file${string || ''}`);
  },

  createFile(key, filename, content) {
    if (!filename || filename === '') {
      return false;
    }

    const payload = {
      key: filename,
      file: content,
    };
    return Api(key).post('/file', JSON.stringify(payload));
  },

  deleteFile(key, filename) {
    if (!filename || filename === '') {
      return false;
    }

    const payload = {
      key: filename,
    };

    return Api(key).delete('/file', { data: JSON.stringify(payload) });
  },
};
