import Api from '@/services/Api';

export default {
  listFolder(key, prefix) {
    const params = [];
    if (prefix) {
      params.push(`prefix=${prefix}`);
    }

    let string;
    if (params.length) {
      string = `?${params.join('&')}`;
    }
    return Api(key).get(`/folder${string || ''}`);
  },
  createFolder(key, folder) {
    if (!folder || folder === '') {
      return false;
    }

    const payload = {
      folderName: folder,
    };
    return Api(key).post('/folder', JSON.stringify(payload));
  },
  deleteFolder(key, folder) {
    if (!folder || folder === '') {
      return false;
    }

    const payload = {
      folderName: folder,
    };

    return Api(key).delete('/folder', { data: JSON.stringify(payload) });
  },
};
