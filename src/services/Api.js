import axios from 'axios';

export default key => axios.create({
  baseURL: 'https://3uuhdyl5s5.execute-api.eu-west-1.amazonaws.com/prod',
  withCredentials: false,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: key,
  },
});
