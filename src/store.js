import Vue from 'vue';
import Vuex from 'vuex';
import Folder from '@/services/FolderService';
import Files from '@/services/FileService';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    loading: false,
    currentFolder: null,
    folders: [],
    contents: [],
    user: {},
    preview: {
      key: '',
      data: {},
    },
    ui: {
      editingNewFolder: false,
      uploadingNewFile: false,
      previewOpen: false,
    },
  },
  actions: {
    async loadFolders({ commit, state }) {
      commit('TOGGLE_LOADING', true);
      try {
        const response = await Folder.listFolder(state.user.token, state.currentFolder);
        commit('HYDRATE_FOLDERS', response.data.CommonPrefixes);
        commit('HYDRATE_CONTENTS', response.data.Contents);
        commit('TOGGLE_LOADING', false);
      } catch (error) {
        commit('TOGGLE_LOADING', false);
        console.error(error);
      }
      return true;
    },
    async createFolder({ commit, state, dispatch }, folder) {
      let result;
      commit('TOGGLE_LOADING', true);
      const folderName = (state.currentFolder || '') + folder;
      try {
        const response = await Folder.createFolder(state.user.token, folderName);
        commit('TOGGLE_LOADING', false);
        if (response.data.ETag) {
          commit('TOGGLE_EDIT_NEW_FOLDER');
          dispatch('loadFolders');
        }
        result = response.data;
      } catch (error) {
        commit('TOGGLE_LOADING', false);
        console.error(error);
        result = error;
      }

      return result;
    },
    async deleteFolder({ commit, state, dispatch }, folder) {
      let result;
      commit('TOGGLE_LOADING', true);
      const folderName = (state.currentFolder || '') + folder;
      try {
        const response = await Folder.deleteFolder(state.user.token, folderName);
        commit('TOGGLE_LOADING', false);
        console.log(response);
        dispatch('loadFolders');
        result = response.data;
      } catch (error) {
        commit('TOGGLE_LOADING', false);
        console.error(error);
        result = error;
      }
      return result;
    },
    async getFile({ commit, state }, filename) {
      commit('TOGGLE_LOADING', true);
      if (state.preview.key === filename) {
        commit('PREVIEW_OPEN');
        commit('TOGGLE_LOADING', false);
        return true;
      }
      try {
        const response = await Files.getFile(state.user.token, filename);
        if (response.status === 200) {
          console.log(response.data);
          commit('PREVIEW_OPEN');
          commit('TOGGLE_LOADING', false);
          commit('HYDRATE_PREVIEW', {
            key: filename,
            data: response.data.Item,
          });
        }
      } catch (error) {
        console.error(error);
        commit('PREVIEW_OPEN');
        commit('TOGGLE_LOADING', false);
        commit('PREVIEW_RESET');
      }
      return true;
    },
    async createFile({ commit, state, dispatch }, file) {
      let result,
        filecontent;
      commit('TOGGLE_LOADING', true);
      const filename = (state.currentFolder || '') + file.name;

      const filereader = new FileReader();
      filereader.onload = async function () {
        filecontent = filereader.result.split(',')[1];
        try {
          const response = await Files.createFile(state.user.token, filename, filecontent);
          commit('TOGGLE_LOADING', false);
          console.log(response);

          if (response.data.ResponseMetadata.RequestId) {
            dispatch('loadFolders');
          }
          return true;
        } catch (error) {
          commit('TOGGLE_LOADING', false);
          console.error(error);
        }
      };
      filereader.readAsDataURL(file);
    },
    async deleteFile({ commit, state, dispatch }, file) {
      let result;
      commit('TOGGLE_LOADING', true);
      const filename = (state.currentFolder || '') + file;
      try {
        const response = await Files.deleteFile(state.user.token, filename);
        commit('TOGGLE_LOADING', false);
        console.log(response);
        dispatch('loadFolders');
        result = response.data;
      } catch (error) {
        commit('TOGGLE_LOADING', false);
        console.error(error);
        result = error;
      }
      return result;
    },
    setPrefix({ commit }, prefix) {
      commit('SET_PREFIX', prefix);
    },
    setUser({ commit }, user) {
      commit('SET_USER', user);
    },
    logout({ commit }) {
      commit('LOGOUT');
    },
    navigateTo({ dispatch }, prefix) {
      dispatch('setPrefix', prefix);
      dispatch('loadFolders');
    },
  },
  mutations: {
    TOGGLE_LOADING(state, status) {
      state.loading = status;
    },
    TOGGLE_EDIT_NEW_FOLDER(state) {
      state.ui.editingNewFolder = !state.ui.editingNewFolder;
    },
    TOGGLE_UPLOAD_NEW_FILE(state) {
      state.ui.uploadingNewFile = !state.ui.uploadingNewFile;
    },
    HYDRATE_FOLDERS(state, folders) {
      if (folders.length > 0) {
        state.folders = folders;
      } else {
        state.folders = [];
      }
    },
    HYDRATE_CONTENTS(state, contents) {
      if (contents.length > 0) {
        state.contents = contents;
      } else {
        state.contents = [];
      }
    },
    SET_PREFIX(state, prefix) {
      state.currentFolder = prefix;
    },
    SET_USER(state, user) {
      state.user = Object.assign({}, user);
    },
    LOGOUT(state) {
      state.user = Object.assign({}, {});
    },
    PREVIEW_OPEN(state) {
      state.ui.previewOpen = true;
    },
    PREVIEW_CLOSE(state) {
      state.ui.previewOpen = false;
    },
    PREVIEW_RESET(state) {
      state.preview = {};
    },
    HYDRATE_PREVIEW(state, payload) {
      state.preview = payload;
    },
  },
  getters: {
    folders: state => state.folders,
    contents: state => state.contents.filter(x => x.Key !== state.currentFolder),
    currentFolder: state => state.currentFolder || '',
    UI: state => state.ui,
    isLoading: state => state.loading,
    isPreviewOpen: state => state.ui.previewOpen,
    isRoot: state => state.currentFolder === ('' || null),
    getUser: state => state.user,
    getPreview: state => state.preview,
    supFolder: (state) => {
      let levelup = state.currentFolder;
      if (!!levelup && levelup !== '/') {
        levelup = levelup.split('/');
        levelup.pop();
        levelup.pop();
        if (levelup.length > 0) {
          return `${levelup.join('/')}/`;
        }
        return null;
      }
      return null;
    },
  },
});
