import config from '@/services/DynamicImagesConfig';

export default {
  methods: {
    previewImageSrc(key, size = '300', direction = 'w', size2 = null) {
      const pathArray = key.split('/');

      const Filename = pathArray.pop();

      const url = [];

      url.push(config.url);
      url.push(pathArray.join('/'));
      url.push(Filename.split('.')[0]);
      
      if (size2) {
        url.push(`${direction}${size}x${size2}`);
      } else {
        url.push(`${direction}${size}`);
      }

      url.push(Filename);

      return url.join('/');
    },
    previewBaseImageSrc(key) {
      const url = [];

      url.push(config.url);
      url.push(key);

      return url.join('/');
    },
  },
};
